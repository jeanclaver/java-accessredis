package accessredis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author JcMoutoh
 */
public  abstract class AccessData {
    
    protected String dbIP = "10.4.0.248" , dbPort = "6379" , dbName = "dbsrver2" , dbPass = "app_user" , dbUser = "app_user";
    
    protected Connection getConnection() throws SQLException,ClassNotFoundException,InstantiationException,IllegalAccessException {

        Class.forName("redis.jdbc.driver.RedisDriver");
        String serverIP = this.dbIP;
        String portNumber = this.dbPort;
        String sid = this.dbName;
        String url = "jdbc:redis://" + serverIP + ":" + portNumber;
        String username = this.dbUser;
        String password = this.dbPass;
        Connection conn = DriverManager.getConnection(url, portNumber, portNumber);
        return conn;
    }
    
    
}


